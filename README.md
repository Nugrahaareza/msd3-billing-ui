# billingadmin

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### To Merge with Spring Gateway

````
-run 'npm run build'
-copy index.html on dist folder to spring-gateway/src/main/resources/templates
-copy css,img,js,favincon.ico folder to spring-gateway/src/main/resources/static
-run mvn clean install on spring gateway project
```
````
