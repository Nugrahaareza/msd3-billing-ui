import Vue from "vue";
import Router from "vue-router";
import store from "@/store/store";

import AuthLayout from "@/layout/AuthLayout";
import MainLayout from "@/layout/MainLayout";
import Login from "./views/authView/Login.vue";
import ResetPasswordReq from "@/views/authView/ResetPasswordReq";
import ResetPassword from "@/views/authView/ResetPassword";

import HomePage from "@/views/mainView/HomePage";
import AuditTrail from "@/views/mainView/audittrail/AuditTrail";
import BillingList from "@/views/mainView/BillingTemplate/BillingList";
import AddBilling from "@/views/mainView/BillingTemplate/AddBilling";
import ApproveBilling from "@/views/mainView/BillingTemplate/ApproveBilling";
import BillingDetails from "@/views/mainView/BillingTemplate/BillingDetails";
import EditBillingScheme from "@/views/mainView/BillingTemplate/EditBillingScheme";
import AppList from "@/views/mainView/BillingTemplate/AppList";
import AllAppsPackage from "@/views/mainView/BillingTemplate/AllAppsPackage";
import Unauthorized from "@/views/mainView/errorPage/Unauthorized";
import InvoiceList from "@/views/mainView/invoice/InvoiceList";
import InvoiceDetail from "@/views/mainView/invoice/InvoiceDetail";
import LogJob from "@/views/mainView/Jobs/LogJob";
import RunJob from "@/views/mainView/Jobs/RunJob";
import ApproveAppScheme from "@/views/mainView/organization/ApproveAppScheme";
import EditAppDetail from "@/views/mainView/organization/EditAppDetail";
import OrganizationAppDetail from "@/views/mainView/organization/OrganizationAppDetail";
import OrganizationDetail from "@/views/mainView/organization/OrganizationDetail";
import OrganizationList from "@/views/mainView/organization/OrganizationList";
import OrganizationScheme from "@/views/mainView/organization/OrganizationScheme";
import OrganizationEdit from "@/views/mainView/organization/OrganizationEdit";
import OrganizationApprove from "@/views/mainView/organization/OrganizationApprove";
import AddRole from "@/views/mainView/roleconfig/AddRole";
import EditRole from "@/views/mainView/roleconfig/EditRole";
import ListRole from "@/views/mainView/roleconfig/ListRole";
import ApproveRole from "@/views/mainView/roleconfig/ApproveRole";
import DetailsRole from "@/views/mainView/roleconfig/DetailsRole";
import Properties from "@/views/mainView/SystemProperties/Properties";
import UserList from "@/views/mainView/user/UserList";
import AddUser from "@/views/mainView/user/AddUser";
import EditUser from "@/views/mainView/user/EditUsers";
import ApproveUser from "@/views/mainView/user/ApproveUser";
import UserDetails from "@/views/mainView/user/UserDetails";
import PaymentTrail from "@/views/mainView/paymentTrail/PaymentTrail";

Vue.use(Router);

let roleData = {};

export default new Router({
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      redirect: "login",
      component: AuthLayout,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem("token") != null) {
          next("/home");
        } else {
          next();
        }
      },
      children: [
        {
          path: "/login",
          name: "Login",
          component: Login,
          props: true,
        },
        {
          path: "/user/password",
          component: ResetPasswordReq,
        },
        {
          path: "/resetPassword/:token",
          name: "ResetPassword",
          component: ResetPassword,
          props: true,
        },
      ],
    },
    {
      path: "/home",
      redirect: "home",
      component: MainLayout,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem("token") === null) {
          next("/login");
        } else {
          next();
          roleData = store.getters;
        }
      },
      children: [
        {
          path: "/home",
          component: HomePage,
        },
        {
          path: "/userlist",
          component: UserList,
          name: "userlist",
          props: true,
        },
        {
          path: "/adduser",
          component: AddUser,
        },
        {
          path: "/edituser/:id",
          props: true,
          component: EditUser,
        },
        {
          path: "/approveUser/:id",
          props: true,
          component: ApproveUser,
          beforeEnter: (to, from, next) => {
            console.log(roleData.isUserChecker);
            if (roleData.isUserChecker) {
              next();
            } else {
              next("/home");
            }
          },
        },
        {
          path: "/userdetails/:id",
          component: UserDetails,
        },
        {
          path: "/roleconfig",
          component: ListRole,
          name: "rolelist",
          props: true,
        },
        {
          path: "/editrole/:id",
          component: EditRole,
        },
        {
          path: "/roledetails/:id",
          component: DetailsRole,
        },
        {
          path: "/addrole",
          component: AddRole,
        },
        {
          path: "/approveRole/:id",
          component: ApproveRole,
          beforeEnter: (to, from, next) => {
            console.log(roleData.isUserChecker);
            if (roleData.isUserChecker) {
              next();
            } else {
              next("/home");
            }
          },
        },
        {
          path: "/systemProperties",
          props: true,
          component: Properties,
        },
        {
          path: "/syncJob",
          props: true,
          component: RunJob,
        },
        {
          path: "/jobTrail",
          props: true,
          component: LogJob,
        },
        {
          path: "/auditTrail",
          props: true,
          component: AuditTrail,
        },
        {
          path: "/paymentTrail",
          props: true,
          component: PaymentTrail,
        },
        {
          path: "/billingScheme",
          component: BillingList,
          name: "billinglist",
          props: true,
        },
        {
          path: "/schemeTemplate/:id",
          component: BillingDetails,
        },
        {
          path: "/addscheme",
          component: AddBilling,
        },
        {
          path: "/editscheme/:id",
          props: true,
          component: EditBillingScheme,
        },
        {
          path: "/usedapplist/:id",
          props: true,
          component: AppList,
        },
        {
          path: "/allappspackage/",
          props: true,
          component: AllAppsPackage,
        },
        {
          path: "/approve/:id",
          props: true,
          component: ApproveBilling,
          beforeEnter: (to, from, next) => {
            console.log(roleData.isChecker);
            if (roleData.isChecker) {
              next();
            } else {
              next("/home");
            }
          },
        },
        {
          path: "/organization",
          props: true,
          component: OrganizationList,
          name: "orgList",
        },
        {
          path: "/editOrganization/:id",
          props: true,
          component: OrganizationEdit,
        },
        {
          path: "/approveOrganization/:id",
          props: true,
          component: OrganizationApprove,
          name: "orgApprove",
        },
        {
          path: "/organizationDetail/:id",
          props: true,
          component: OrganizationDetail,
          name: "orgDetail",
        },
        {
          path: "/organizationScheme/:id",
          props: true,
          component: OrganizationScheme,
        },
        {
          path: "/organizationappScheme/:id",
          props: true,
          component: OrganizationAppDetail,
        },
        {
          path: "/organizationappapprove/:id",
          props: true,
          name: "orgApproveScheme",
          component: ApproveAppScheme,
          beforeEnter: (to, from, next) => {
            console.log(roleData.isChecker);
            if (roleData.isChecker) {
              next();
            } else {
              next("/home");
            }
          },
        },
        {
          path: "/editOrgAppScheme/:id",
          props: true,
          component: EditAppDetail,
        },
        {
          path: "/invoice",
          props: true,
          component: InvoiceList,
        },
        {
          path: "/invoiceDetail/:id",
          props: true,
          component: InvoiceDetail,
        },
        {
          path: "/unauthorized",
          props: true,
          component: Unauthorized,
        },
      ],
    },
    {
      path: "*",
      redirect: "home",
      component: MainLayout,
    },
  ],
  mode: "hash",
});
