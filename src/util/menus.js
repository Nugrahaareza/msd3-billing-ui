const menuadmin=[
    {
        link:"/billingScheme",
        label:"Billing Scheme"
    },
    {
        link:"/organization",
        label:"Customer"
    },
    {
        link:"/userList",
        label:"User List"
    },
    {
        link:"/auditTrail",
        label:"Audit Trail"
    },
]

export default menuadmin;