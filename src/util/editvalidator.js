const validateEdit = async (oldData, newData, submitData, formType) => {
  let notvalidated = true;
  switch (formType) {
    case "User":
      notvalidated = userValidate(oldData, newData, submitData);
      return notvalidated;
    case "Organization":
      notvalidated = organizaitonValidate(oldData, newData, submitData);
      return notvalidated;
    default:
      return false;
  }
};

const organizaitonValidate = async (oldData, newData, submitData) => {
  let notvalid = true;

  if (newData === null) {
    console.log(oldData.notificationEmail === submitData.notificationEmail);
    console.log(oldData.notificationName);
    console.log(submitData.notificationName);

    console.log(oldData.notificationName === submitData.notificationName);
    notvalid =
      oldData.notificationEmail === submitData.notificationEmail && notvalid;
    notvalid =
      oldData.notificationName === submitData.notificationName && notvalid;
    return notvalid;
  } else if (newData !== null) {
    notvalid =
      newData.notificationEmail === submitData.notificationEmail && notvalid;
    notvalid =
      newData.notificationName === submitData.notificationName && notvalid;
    return notvalid;
  }
};

const userValidate = async (oldData, newData, submitData) => {
  let notvalid = true;
  if (newData === null) {
    const emailSubmit = submitData.email;
    const emailCurrent = oldData.email;
    notvalid = emailSubmit === emailCurrent && notvalid;
    let roleCurrent = [];
    let roleSubmit = [];
    if (oldData.roles) {
      await oldData.roles.map((role) => {
        if (role.id) {
          roleCurrent.push(role.id);
        } else {
          roleCurrent.push(role);
        }
      });
    }
    if (submitData.roles) {
      await submitData.roles.map((role) => {
        roleSubmit.push(role.id);
      });
    }
    /* WARNING: arrays must not contain {objects} or behavior may be undefined */
    notvalid =
      JSON.stringify(roleSubmit) == JSON.stringify(roleCurrent) && notvalid;
    console.log(notvalid);
    return notvalid;
  } else if (newData) {
    const emailSubmit = submitData.email;
    const emailCurrent = newData.email;
    notvalid = emailSubmit === emailCurrent && notvalid;
    let roleCurrent = [];
    let roleSubmit = [];
    if (newData.roles) {
      await newData.roles.map((role) => {
        if (role.id) {
          roleCurrent.push(role.id);
        } else {
          roleCurrent.push(role);
        }
      });
    }
    if (submitData.roles) {
      await submitData.roles.map((role) => {
        roleSubmit.push(role.id);
      });
    }
    /* WARNING: arrays must not contain {objects} or behavior may be undefined */
    notvalid =
      JSON.stringify(roleSubmit) == JSON.stringify(roleCurrent) && notvalid;
    console.log(notvalid);
    return notvalid;
  }
};

export default validateEdit;
