const validate = async (inputdata) => {
  console.log(inputdata);
  if (!inputdata.forApps) {
    if (inputdata.prodLength < 1) {
      return {
        err: true,
        errMsg: "Please add template name first",
      };
    }
  }

  if (inputdata.chargePerCall !== null && inputdata.maxCall !== null) {
    let isValid = true;
    let isValidMaxTier = true;

    const chargeRegex = new RegExp(/^[1-9][0-9]*$/g);
    const maxCallRegex = new RegExp(/^[1-9][0-9]*$/g);

    const cpc = inputdata.chargePerCall.toString();
    const mc = inputdata.maxCall.toString();
    console.log(mc);
    isValid = await chargeRegex.test(cpc.trim());
    console.log(isValid);
    if (!inputdata.isMaxTier) {
      // console.log(await maxCallRegex.test(mc.trim()));
      isValidMaxTier = await maxCallRegex.test(mc.trim());
      console.log(isValidMaxTier);
    }

    if ((!isValid || !isValidMaxTier) && inputdata.forEdit) {
      return { errEdit: true, errEditMsg: "Invalid input value" };
    } else if (!isValid || !isValidMaxTier) {
      return { err: true, errMsg: "Invalid input value" };
    } else {
      return false;
    }
  }
};

export default validate;
