const getDefaultState = () => {
  return {
    tierData: [],
    oldtierData: [],
  };
};

const state = getDefaultState();

const mutations = {
  GET_FROM_SERVER(state, data) {
    state.tierData = data;
  },
  GET_Old_FROM_SERVER(state, data) {
    state.oldtierData = data;
  },

  SET_TIERDATA(state, data) {
    console.log(data);
    state.tierData.push(data);
    console.log(state);
  },

  SET_OLDTIERDATA(state, data) {
    console.log(data);
    state.oldtierData.push(data);
    console.log(state.oldtierData);
  },

  UPDATE_TIERDATA(state, data) {
    state.tierData.map((st) => {
      if (st.billingTemplateLvl === data.billingTemplateLvl) {
        st.billingCharge = data.chargePerCall;
        st.chargePerCall = data.chargePerCall;
        st.billingTemplateCharge = data.chargePerCall;
        st.isMaxTier = data.isMaxTier;
        st.maxCall = data.maxCall;
      }
      console.log(state.tierData);
    });
  },

  UPDATE_OLDTIERDATA(state, data) {
    console.log(data);
    state.oldtierData.map((st) => {
      console.log(st);
      if (st.billingTemplateLvl) {
        console.log("template");
        if (st.billingTemplateLvl === data.billingTemplateLvl) {
          st.billingCharge = data.chargePerCall;
          st.chargePerCall = data.chargePerCall;
          st.billingTemplateCharge = data.chargePerCall;
          st.isMaxTier = data.isMaxTier;
          st.maxCall = data.maxCall;
        } else if (st.billingTierLvl) {
          console.log("template fake");
          if (st.billingTierLvl === data.billingTemplateLvl) {
            st.billingCharge = data.chargePerCall;
            st.chargePerCall = data.chargePerCall;
            st.billingTemplateCharge = data.chargePerCall;
            st.isMaxTier = data.isMaxTier;
            st.maxCall = data.maxCall;
          }
        }
      }
      //console.log(state.oldtierData);
    });
  },

  DELETE_TIERDATA(state, { lvl }) {
    state.tierData = state.tierData.filter((obj) => {
      if (obj.billingTemplateLvl) {
        return obj.billingTemplateLvl !== lvl;
      } else {
        return obj.billingTierLvl !== lvl;
      }
    });
    for (var i = 0; i < state.tierData.length; i++) {
      if (state.tierData.billingTemplateLvl) {
        state.tierData[i].billingTemplateLvl = i + 1;
      } else {
        state.tierData[i].billingTierLvl = i + 1;
      }
    }
  },

  DELETE_OLDTIERDATA(state, { lvl }) {
    state.oldtierData = state.oldtierData.filter((obj) => {
      if (obj.billingTemplateLvl) {
        return obj.billingTemplateLvl !== lvl;
      } else {
        return obj.billingTierLvl !== lvl;
      }
    });
    for (var i = 0; i < state.oldtierData.length; i++) {
      const reqlvl =
        state.oldtierData.billingTemplateLvl ||
        state.oldtierData.billingTierLvl;
      if (reqlvl) {
        state.oldtierData[i].billingTemplateLvl = i + 1;
      } else {
        state.oldtierData[i].billingTierLvl = i + 1;
      }
    }
  },

  CLEAR_TIERDATA(state) {
    console.log("clearing....");
    Object.assign(state, getDefaultState());
  },
};

const actions = {
  getDataFromServer: ({ commit }, order) => {
    commit("GET_FROM_SERVER", order);
  },
  getOldDataFromServer: ({ commit }, order) => {
    commit("GET_Old_FROM_SERVER", order);
  },
  setTierData: ({ commit }, order) => {
    commit("SET_TIERDATA", order);
  },
  setOldTierData: ({ commit }, order) => {
    commit("SET_OLDTIERDATA", order);
  },

  updateTierData: ({ commit }, order) => {
    commit("UPDATE_TIERDATA", order);
  },

  deleteTierData: ({ commit }, order) => {
    commit("DELETE_TIERDATA", order);
  },

  updateOldTierData: ({ commit }, order) => {
    commit("UPDATE_OLDTIERDATA", order);
  },

  deleteOldTierData: ({ commit }, order) => {
    commit("DELETE_OLDTIERDATA", order);
  },

  clearTierData({ commit }) {
    commit("CLEAR_TIERDATA");
  },
};

const getters = {
  tierData: (state) => {
    return state.tierData;
  },
  oldTierData: (state) => {
    return state.oldtierData;
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
