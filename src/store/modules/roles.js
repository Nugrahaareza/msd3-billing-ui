import jwt from "jsonwebtoken";

const getDefaultState = () => {
  return {
    isChecker: false,
    isUserChecker: false,
    isUserMaker: false,
  };
};

const state = getDefaultState();

const mutations = {
  CHECK_ROLES(state) {
    const token = jwt.decode(localStorage.getItem("token"));
    let role = token.role[0].toUpperCase();
    role = role.replace(/\s/g, "");
    console.log(role);
    switch (role) {
      case "BILLINGMANAGER":
        state.isChecker = true;
        break;
      case "SECURITYOFFICER":
        state.isUserMaker = true;
        break;
      case "SECURITYMANAGER":
        state.isUserChecker = true;
        break;
    }
  },
  CLEAR_ROLES(state) {
    Object.assign(state, getDefaultState());
  },
};

const actions = {
  checkRoles: ({ commit }) => {
    commit("CHECK_ROLES");
  },
  removeRoles: ({ commit }) => {
    commit("CLEAR_ROLES");
  },
};

const getters = {
  isChecker: (state) => {
    return state.isChecker;
  },
  isUserChecker: (state) => {
    return state.isUserChecker;
  },
  isUserMaker: (state) => {
    return state.isUserMaker;
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
