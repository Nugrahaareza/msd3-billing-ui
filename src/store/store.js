import Vue from "vue";
import Vuex from "vuex";
//import jwt from "jsonwebtoken";
import roles from "@/store/modules/roles";
import tierTable from "@/store/modules/tierTable";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    roles,
    tierTable,
  },
  //state: {
  //  isChecker: false,
  //},
  //getters: {
  //  getRole: (state) => {
  //    const token = jwt.decode(localStorage.getItem("token"));
  //    console.log(token.role[0].toUpperCase());
  //    if (token.role[0].toUpperCase() === "ROLE_CHECKER") {
  //      state.isChecker = true;
  //      console.log(state.isChecker);
  //    }
  //    return state.isChecker;
  //  },
  //},
  //mutations: {
  //  clearChecker() {
  //    this.isChecker = false;
  //  },
});
