import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Autocomplete from "@trevoreyre/autocomplete-vue";
import "@trevoreyre/autocomplete-vue/dist/style.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";

import store from "./store/store";

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(Autocomplete);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
